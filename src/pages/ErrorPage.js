import Banner from "../components/Banner";
import Error from "../components/Error";

export default function ErrorPage() {
  return (
    <>
      <Error />
    </>
  );
}
