import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";

export default function Courses() {
  //   console.log(coursesData);
  //   console.log(coursesData[0]);
  const courses = coursesData.map((course) => {
    return <CourseCard key={course.id} props={course} />;
    //key identifies what elements inside map, for individuality
  });
  return (
    <div>
      <h1>Courses Available: </h1>
      {courses}
    </div>
  );
}
