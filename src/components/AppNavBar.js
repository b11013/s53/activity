import { Navbar, Container, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState } from "react";

export default function AppNavBar() {
  const [user, setUser] = useState(localStorage.getItem("email"));

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">
          {" "}
          Booking App{" "}
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">
              {" "}
              Home{" "}
            </Nav.Link>
            <Nav.Link as={Link} to="/courses">
              {" "}
              Courses{" "}
            </Nav.Link>

            {user !== null ? (
              <Nav.Link as={Link} to="/logout">
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={Link} to="/login">
                  {" "}
                  Login{" "}
                </Nav.Link>
                <Nav.Link as={Link} to="/register">
                  {" "}
                  Register{" "}
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
