import { Card, Nav } from "react-bootstrap";
import Image from "./404.jpg";
import { Link } from "react-router-dom";

export default function Error() {
  return (
    <div>
      <Card>
        <Nav.Link as={Link} to="/">
          <Card.Img variant="top" src={Image} />
        </Nav.Link>
      </Card>
    </div>
  );
}
